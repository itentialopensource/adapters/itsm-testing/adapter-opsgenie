
## 0.3.4 [10-15-2024]

* Changes made at 2024.10.14_20:05PM

See merge request itentialopensource/adapters/adapter-opsgenie!11

---

## 0.3.3 [09-13-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-opsgenie!9

---

## 0.3.2 [08-14-2024]

* Changes made at 2024.08.14_18:13PM

See merge request itentialopensource/adapters/adapter-opsgenie!8

---

## 0.3.1 [08-07-2024]

* Changes made at 2024.08.06_19:26PM

See merge request itentialopensource/adapters/adapter-opsgenie!7

---

## 0.3.0 [07-05-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/itsm-testing/adapter-opsgenie!6

---

## 0.2.4 [03-27-2024]

* Changes made at 2024.03.27_13:59PM

See merge request itentialopensource/adapters/itsm-testing/adapter-opsgenie!5

---

## 0.2.3 [03-13-2024]

* Changes made at 2024.03.13_15:02PM

See merge request itentialopensource/adapters/itsm-testing/adapter-opsgenie!4

---

## 0.2.2 [03-11-2024]

* Changes made at 2024.03.11_14:43PM

See merge request itentialopensource/adapters/itsm-testing/adapter-opsgenie!3

---

## 0.2.1 [02-28-2024]

* Changes made at 2024.02.28_11:07AM

See merge request itentialopensource/adapters/itsm-testing/adapter-opsgenie!2

---

## 0.2.0 [12-31-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/itsm-testing/adapter-opsgenie!1

---

## 0.1.1 [06-08-2022]

* Bug fixes and performance improvements

See commit e651dcb

---
