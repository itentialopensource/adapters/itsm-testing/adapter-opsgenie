## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Opsgenie. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Opsgenie.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Atlassian Opsgenie. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getRequestStatus(requestId, callback)</td>
    <td style="padding:15px">Get Request Status of Alert</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/requests/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAlerts(query, searchIdentifier, searchIdentifierType = 'id', offset, limit, sort = 'createdAt', order = 'asc', callback)</td>
    <td style="padding:15px">List Alerts</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAlert(body, callback)</td>
    <td style="padding:15px">Create Alert</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlert(identifier, identifierType = 'id', callback)</td>
    <td style="padding:15px">Get Alert</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAlert(identifier, identifierType = 'id', user, source, callback)</td>
    <td style="padding:15px">Delete Alert</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acknowledgeAlert(identifier, identifierType = 'id', body, callback)</td>
    <td style="padding:15px">Acknowledge Alert</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/{pathv1}/acknowledge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unAcknowledgeAlert(identifier, identifierType = 'id', body, callback)</td>
    <td style="padding:15px">UnAcknowledge Alert</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/{pathv1}/unacknowledge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">closeAlert(identifier, identifierType = 'id', body, callback)</td>
    <td style="padding:15px">Close Alert</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/{pathv1}/close?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">snoozeAlert(identifier, identifierType = 'id', body, callback)</td>
    <td style="padding:15px">Snooze Alert</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/{pathv1}/snooze?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">escalateAlert(identifier, identifierType = 'id', body, callback)</td>
    <td style="padding:15px">Escalate Alert</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/{pathv1}/escalate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignAlert(identifier, identifierType = 'id', body, callback)</td>
    <td style="padding:15px">Assign Alert</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/{pathv1}/assign?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addResponder(identifier, identifierType = 'id', body, callback)</td>
    <td style="padding:15px">Add Responder</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/{pathv1}/responders?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addTeam(identifier, identifierType = 'id', body, callback)</td>
    <td style="padding:15px">Add Team</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/{pathv1}/teams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">executeCustomAlertAction(identifier, identifierType = 'id', actionName, body, callback)</td>
    <td style="padding:15px">Custom Alert Action</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/{pathv1}/actions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRecipients(identifier, identifierType = 'id', callback)</td>
    <td style="padding:15px">List Alert Recipients</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/{pathv1}/recipients?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listLogs(identifier, identifierType = 'id', offset, direction = 'next', limit, order = 'asc', callback)</td>
    <td style="padding:15px">List Alert Logs</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/{pathv1}/logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAttachments(identifier, alertIdentifierType = 'id', callback)</td>
    <td style="padding:15px">List Alert Attachments</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/{pathv1}/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addAttachment(identifier, alertIdentifierType = 'id', file, user, indexFile, callback)</td>
    <td style="padding:15px">Add Alert Attachment</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/{pathv1}/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAttachment(identifier, alertIdentifierType = 'id', attachmentId, callback)</td>
    <td style="padding:15px">Get Alert Attachment</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/{pathv1}/attachments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeAttachment(identifier, alertIdentifierType = 'id', attachmentId, user, callback)</td>
    <td style="padding:15px">Remove Alert Attachment</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/{pathv1}/attachments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addTags(identifier, identifierType = 'id', body, callback)</td>
    <td style="padding:15px">Add Tags</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/{pathv1}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeTags(identifier, identifierType = 'id', user, note, source, tags, callback)</td>
    <td style="padding:15px">Remove Tags</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/{pathv1}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDetails(identifier, identifierType = 'id', body, callback)</td>
    <td style="padding:15px">Add Details</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/{pathv1}/details?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeDetails(identifier, identifierType = 'id', user, note, source, keys, callback)</td>
    <td style="padding:15px">Remove Details</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/{pathv1}/details?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listNotes(identifier, identifierType = 'id', offset, direction = 'next', limit, order = 'asc', callback)</td>
    <td style="padding:15px">List Alert Notes</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/{pathv1}/notes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addNote(identifier, identifierType = 'id', body, callback)</td>
    <td style="padding:15px">Add Note</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/{pathv1}/notes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSavedSearches(callback)</td>
    <td style="padding:15px">Lists Saved Searches</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/saved-searches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSavedSearches(body, callback)</td>
    <td style="padding:15px">Create Saved Search</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/saved-searches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSavedSearch(identifier, identifierType = 'id', callback)</td>
    <td style="padding:15px">Get Saved Search</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/saved-searches/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSavedSearch(identifier, identifierType = 'id', callback)</td>
    <td style="padding:15px">Delete Saved Search</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/saved-searches/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSavedSearch(identifier, identifierType = 'id', body, callback)</td>
    <td style="padding:15px">Update Saved Search</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/saved-searches/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countAlerts(query, searchIdentifier, searchIdentifierType = 'id', callback)</td>
    <td style="padding:15px">Count Alerts</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAlertMessage(identifier, identifierType = 'id', body, callback)</td>
    <td style="padding:15px">Update Alert Message</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/{pathv1}/message?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAlertDescription(identifier, identifierType = 'id', body, callback)</td>
    <td style="padding:15px">Update Alert Description</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/{pathv1}/description?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAlertPriority(identifier, identifierType = 'id', body, callback)</td>
    <td style="padding:15px">Update Alert Priority</td>
    <td style="padding:15px">{base_path}/{version}/v2/alerts/{pathv1}/priority?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listIntegrations(type, teamId, teamName, callback)</td>
    <td style="padding:15px">List Integrations</td>
    <td style="padding:15px">{base_path}/{version}/v2/integrations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIntegration(body, callback)</td>
    <td style="padding:15px">Create Integration</td>
    <td style="padding:15px">{base_path}/{version}/v2/integrations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIntegration(id, callback)</td>
    <td style="padding:15px">Get Integration</td>
    <td style="padding:15px">{base_path}/{version}/v2/integrations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIntegration(id, body, callback)</td>
    <td style="padding:15px">Update Integration</td>
    <td style="padding:15px">{base_path}/{version}/v2/integrations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIntegration(id, callback)</td>
    <td style="padding:15px">Delete Integration</td>
    <td style="padding:15px">{base_path}/{version}/v2/integrations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableIntegration(id, callback)</td>
    <td style="padding:15px">Enable Integration</td>
    <td style="padding:15px">{base_path}/{version}/v2/integrations/{pathv1}/enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableIntegration(id, callback)</td>
    <td style="padding:15px">Disable Integration</td>
    <td style="padding:15px">{base_path}/{version}/v2/integrations/{pathv1}/disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">authenticateIntegration(body, callback)</td>
    <td style="padding:15px">Authenticate Integration</td>
    <td style="padding:15px">{base_path}/{version}/v2/integrations/authenticate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listIntegrationActions(id, callback)</td>
    <td style="padding:15px">List Integration Actions</td>
    <td style="padding:15px">{base_path}/{version}/v2/integrations/{pathv1}/actions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIntegrationActions(id, body, callback)</td>
    <td style="padding:15px">Update Integration Actions</td>
    <td style="padding:15px">{base_path}/{version}/v2/integrations/{pathv1}/actions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIntegrationAction(id, body, callback)</td>
    <td style="padding:15px">Create Integration Action</td>
    <td style="padding:15px">{base_path}/{version}/v2/integrations/{pathv1}/actions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ping(name, callback)</td>
    <td style="padding:15px">Ping Heartbeat</td>
    <td style="padding:15px">{base_path}/{version}/v2/heartbeats/{pathv1}/ping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listHeartBeats(callback)</td>
    <td style="padding:15px">List Heartbeats</td>
    <td style="padding:15px">{base_path}/{version}/v2/heartbeats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createHeartbeat(body, callback)</td>
    <td style="padding:15px">Create Heartbeat</td>
    <td style="padding:15px">{base_path}/{version}/v2/heartbeats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHeartbeat(name, callback)</td>
    <td style="padding:15px">Get Heartbeat</td>
    <td style="padding:15px">{base_path}/{version}/v2/heartbeats/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHeartbeat(name, body, callback)</td>
    <td style="padding:15px">Update Heartbeat (Partial)</td>
    <td style="padding:15px">{base_path}/{version}/v2/heartbeats/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHeartbeat(name, callback)</td>
    <td style="padding:15px">Delete Heartbeat</td>
    <td style="padding:15px">{base_path}/{version}/v2/heartbeats/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableHeartbeat(name, callback)</td>
    <td style="padding:15px">Enable Heartbeat</td>
    <td style="padding:15px">{base_path}/{version}/v2/heartbeats/{pathv1}/enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableHeartbeat(name, callback)</td>
    <td style="padding:15px">Disable Heartbeat</td>
    <td style="padding:15px">{base_path}/{version}/v2/heartbeats/{pathv1}/disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAlertPolicies(callback)</td>
    <td style="padding:15px">List Alert Policies</td>
    <td style="padding:15px">{base_path}/{version}/v1/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAlertPolicy(body, callback)</td>
    <td style="padding:15px">Create Alert Policy</td>
    <td style="padding:15px">{base_path}/{version}/v1/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertPolicy(policyId, callback)</td>
    <td style="padding:15px">Get Alert Policy</td>
    <td style="padding:15px">{base_path}/{version}/v1/policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAlertPolicy(policyId, body, callback)</td>
    <td style="padding:15px">Update Alert Policy</td>
    <td style="padding:15px">{base_path}/{version}/v1/policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAlertPolicy(policyId, callback)</td>
    <td style="padding:15px">Delete Alert Policy</td>
    <td style="padding:15px">{base_path}/{version}/v1/policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableAlertPolicy(policyId, callback)</td>
    <td style="padding:15px">Enable Alert Policy</td>
    <td style="padding:15px">{base_path}/{version}/v1/policies/{pathv1}/enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableAlertPolicy(policyId, callback)</td>
    <td style="padding:15px">Disable Alert Policy</td>
    <td style="padding:15px">{base_path}/{version}/v1/policies/{pathv1}/disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeAlertPolicyOrder(policyId, body, callback)</td>
    <td style="padding:15px">Change Alert Policy Order</td>
    <td style="padding:15px">{base_path}/{version}/v1/policies/{pathv1}/change-order?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMaintenance(body, callback)</td>
    <td style="padding:15px">Create Maintenance</td>
    <td style="padding:15px">{base_path}/{version}/v1/maintenance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listMaintenance(type = 'all', callback)</td>
    <td style="padding:15px">List Maintenance</td>
    <td style="padding:15px">{base_path}/{version}/v1/maintenance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMaintenance(id, callback)</td>
    <td style="padding:15px">Get Maintenance</td>
    <td style="padding:15px">{base_path}/{version}/v1/maintenance/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMaintenance(id, body, callback)</td>
    <td style="padding:15px">Update Maintenance</td>
    <td style="padding:15px">{base_path}/{version}/v1/maintenance/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMaintenance(id, callback)</td>
    <td style="padding:15px">Delete Maintenance</td>
    <td style="padding:15px">{base_path}/{version}/v1/maintenance/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelMaintenance(id, callback)</td>
    <td style="padding:15px">Cancel Maintenance</td>
    <td style="padding:15px">{base_path}/{version}/v1/maintenance/{pathv1}/cancel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInfo(callback)</td>
    <td style="padding:15px">Get Account Info</td>
    <td style="padding:15px">{base_path}/{version}/v2/account?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUser(body, callback)</td>
    <td style="padding:15px">Create User</td>
    <td style="padding:15px">{base_path}/{version}/v2/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listUsers(limit, offset, sortField, order = 'asc', query, callback)</td>
    <td style="padding:15px">List users</td>
    <td style="padding:15px">{base_path}/{version}/v2/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUser(identifier, expand, callback)</td>
    <td style="padding:15px">Get User</td>
    <td style="padding:15px">{base_path}/{version}/v2/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUser(identifier, body, callback)</td>
    <td style="padding:15px">Update User (Partial)</td>
    <td style="padding:15px">{base_path}/{version}/v2/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUser(identifier, callback)</td>
    <td style="padding:15px">Delete User</td>
    <td style="padding:15px">{base_path}/{version}/v2/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listUserTeams(identifier, callback)</td>
    <td style="padding:15px">List User Teams</td>
    <td style="padding:15px">{base_path}/{version}/v2/users/{pathv1}/teams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listUserForwardingRules(identifier, callback)</td>
    <td style="padding:15px">List User Forwarding Rules</td>
    <td style="padding:15px">{base_path}/{version}/v2/users/{pathv1}/forwarding-rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listUserEscalations(identifier, callback)</td>
    <td style="padding:15px">List User Escalations</td>
    <td style="padding:15px">{base_path}/{version}/v2/users/{pathv1}/escalations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listUserSchedules(identifier, callback)</td>
    <td style="padding:15px">List User Schedules</td>
    <td style="padding:15px">{base_path}/{version}/v2/users/{pathv1}/schedules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createContact(identifier, body, callback)</td>
    <td style="padding:15px">Create Contact</td>
    <td style="padding:15px">{base_path}/{version}/v2/users/{pathv1}/contacts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listContacts(identifier, callback)</td>
    <td style="padding:15px">List Contacts</td>
    <td style="padding:15px">{base_path}/{version}/v2/users/{pathv1}/contacts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateContact(identifier, contactId, body, callback)</td>
    <td style="padding:15px">Update Contact (Partial)</td>
    <td style="padding:15px">{base_path}/{version}/v2/users/{pathv1}/contacts/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getContact(identifier, contactId, callback)</td>
    <td style="padding:15px">Get Contact</td>
    <td style="padding:15px">{base_path}/{version}/v2/users/{pathv1}/contacts/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteContact(identifier, contactId, callback)</td>
    <td style="padding:15px">Delete Contact</td>
    <td style="padding:15px">{base_path}/{version}/v2/users/{pathv1}/contacts/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableContact(identifier, contactId, callback)</td>
    <td style="padding:15px">Enable Contact</td>
    <td style="padding:15px">{base_path}/{version}/v2/users/{pathv1}/contacts/{pathv2}/enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableContact(identifier, contactId, callback)</td>
    <td style="padding:15px">Disable Contact</td>
    <td style="padding:15px">{base_path}/{version}/v2/users/{pathv1}/contacts/{pathv2}/disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listNotificationRules(identifier, callback)</td>
    <td style="padding:15px">List Notification Rules</td>
    <td style="padding:15px">{base_path}/{version}/v2/users/{pathv1}/notification-rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNotificationRule(identifier, body, callback)</td>
    <td style="padding:15px">Create Notification Rule</td>
    <td style="padding:15px">{base_path}/{version}/v2/users/{pathv1}/notification-rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNotificationRule(identifier, ruleId, callback)</td>
    <td style="padding:15px">Get Notification Rule</td>
    <td style="padding:15px">{base_path}/{version}/v2/users/{pathv1}/notification-rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNotificationRule(identifier, ruleId, callback)</td>
    <td style="padding:15px">Delete Notification Rule</td>
    <td style="padding:15px">{base_path}/{version}/v2/users/{pathv1}/notification-rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNotificationRule(identifier, ruleId, body, callback)</td>
    <td style="padding:15px">Update Notification Rule (Partial)</td>
    <td style="padding:15px">{base_path}/{version}/v2/users/{pathv1}/notification-rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableNotificationRule(identifier, ruleId, callback)</td>
    <td style="padding:15px">Enable Notification Rule</td>
    <td style="padding:15px">{base_path}/{version}/v2/users/{pathv1}/notification-rules/{pathv2}/enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableNotificationRule(identifier, ruleId, callback)</td>
    <td style="padding:15px">Disable Notification Rule</td>
    <td style="padding:15px">{base_path}/{version}/v2/users/{pathv1}/notification-rules/{pathv2}/disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeNotificationRuleOrder(identifier, ruleId, body, callback)</td>
    <td style="padding:15px">Change order of Notification Rule</td>
    <td style="padding:15px">{base_path}/{version}/v2/users/{pathv1}/notification-rules/{pathv2}/change-order?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listNotificationRuleSteps(identifier, ruleId, callback)</td>
    <td style="padding:15px">List Notification Rule Steps</td>
    <td style="padding:15px">{base_path}/{version}/v2/users/{pathv1}/notification-rules/{pathv2}/steps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNotificationRuleStep(identifier, ruleId, body, callback)</td>
    <td style="padding:15px">Create Notification Rule Step</td>
    <td style="padding:15px">{base_path}/{version}/v2/users/{pathv1}/notification-rules/{pathv2}/steps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNotificationRuleStep(identifier, ruleId, id, callback)</td>
    <td style="padding:15px">Get Notification Rule Step</td>
    <td style="padding:15px">{base_path}/{version}/v2/users/{pathv1}/notification-rules/{pathv2}/steps/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNotificationRuleStep(identifier, ruleId, id, callback)</td>
    <td style="padding:15px">Delete Notification Rule Step</td>
    <td style="padding:15px">{base_path}/{version}/v2/users/{pathv1}/notification-rules/{pathv2}/steps/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNotificationRuleStep(identifier, ruleId, id, body, callback)</td>
    <td style="padding:15px">Update Notification Rule Step (Partial)</td>
    <td style="padding:15px">{base_path}/{version}/v2/users/{pathv1}/notification-rules/{pathv2}/steps/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableNotificationRuleStep(identifier, ruleId, id, callback)</td>
    <td style="padding:15px">Disable Notification Rule Step</td>
    <td style="padding:15px">{base_path}/{version}/v2/users/{pathv1}/notification-rules/{pathv2}/steps/{pathv3}/disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableNotificationRuleStep(identifier, ruleId, id, callback)</td>
    <td style="padding:15px">Enable Notification Rule Step</td>
    <td style="padding:15px">{base_path}/{version}/v2/users/{pathv1}/notification-rules/{pathv2}/steps/{pathv3}/enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTeam(body, callback)</td>
    <td style="padding:15px">Create Team</td>
    <td style="padding:15px">{base_path}/{version}/v2/teams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTeams(callback)</td>
    <td style="padding:15px">List Teams</td>
    <td style="padding:15px">{base_path}/{version}/v2/teams?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTeam(identifier, identifierType = 'id', callback)</td>
    <td style="padding:15px">Get Team</td>
    <td style="padding:15px">{base_path}/{version}/v2/teams/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTeam(identifier, identifierType = 'id', callback)</td>
    <td style="padding:15px">Delete Team</td>
    <td style="padding:15px">{base_path}/{version}/v2/teams/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTeam(identifier, body, callback)</td>
    <td style="padding:15px">Update Team (Partial)</td>
    <td style="padding:15px">{base_path}/{version}/v2/teams/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTeamLogs(identifier, identifierType = 'id', limit, order = 'asc', offset, callback)</td>
    <td style="padding:15px">List Team Logs</td>
    <td style="padding:15px">{base_path}/{version}/v2/teams/{pathv1}/logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addTeamMember(identifier, teamIdentifierType = 'id', body, callback)</td>
    <td style="padding:15px">Add Team Member</td>
    <td style="padding:15px">{base_path}/{version}/v2/teams/{pathv1}/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTeamMember(identifier, teamIdentifierType = 'id', memberIdentifier, callback)</td>
    <td style="padding:15px">Delete Team Member</td>
    <td style="padding:15px">{base_path}/{version}/v2/teams/{pathv1}/members/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTeamRoles(identifier, teamIdentifierType = 'id', callback)</td>
    <td style="padding:15px">List Team Roles</td>
    <td style="padding:15px">{base_path}/{version}/v2/teams/{pathv1}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTeamRole(identifier, teamIdentifierType = 'id', body, callback)</td>
    <td style="padding:15px">Create Team Role</td>
    <td style="padding:15px">{base_path}/{version}/v2/teams/{pathv1}/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTeamRole(identifier, teamIdentifierType = 'id', teamRoleIdentifier, identifierType = 'id', callback)</td>
    <td style="padding:15px">Get Team Role</td>
    <td style="padding:15px">{base_path}/{version}/v2/teams/{pathv1}/roles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTeamRole(identifier, teamIdentifierType = 'id', teamRoleIdentifier, identifierType = 'id', callback)</td>
    <td style="padding:15px">Delete Team Role</td>
    <td style="padding:15px">{base_path}/{version}/v2/teams/{pathv1}/roles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTeamRole(identifier, teamIdentifierType = 'id', teamRoleIdentifier, identifierType = 'id', body, callback)</td>
    <td style="padding:15px">Update Team Role (Partial)</td>
    <td style="padding:15px">{base_path}/{version}/v2/teams/{pathv1}/roles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTeamRoutingRule(identifier, teamIdentifierType = 'id', body, callback)</td>
    <td style="padding:15px">Create Team Routing Rule</td>
    <td style="padding:15px">{base_path}/{version}/v2/teams/{pathv1}/routing-rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTeamRoutingRules(identifier, teamIdentifierType = 'id', callback)</td>
    <td style="padding:15px">List Team Routing Rules</td>
    <td style="padding:15px">{base_path}/{version}/v2/teams/{pathv1}/routing-rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTeamRoutingRule(identifier, teamIdentifierType = 'id', id, callback)</td>
    <td style="padding:15px">Get Team Routing Rule</td>
    <td style="padding:15px">{base_path}/{version}/v2/teams/{pathv1}/routing-rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTeamRoutingRule(identifier, teamIdentifierType = 'id', id, body, callback)</td>
    <td style="padding:15px">Update Team Routing Rule (Partial)</td>
    <td style="padding:15px">{base_path}/{version}/v2/teams/{pathv1}/routing-rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTeamRoutingRule(identifier, teamIdentifierType = 'id', id, callback)</td>
    <td style="padding:15px">Delete Team Routing Rule</td>
    <td style="padding:15px">{base_path}/{version}/v2/teams/{pathv1}/routing-rules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeTeamRoutingRuleOrder(identifier, teamIdentifierType = 'id', id, body, callback)</td>
    <td style="padding:15px">Change Team Routing Rule Order</td>
    <td style="padding:15px">{base_path}/{version}/v2/teams/{pathv1}/routing-rules/{pathv2}/change-order?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSchedules(expand, callback)</td>
    <td style="padding:15px">List Schedules</td>
    <td style="padding:15px">{base_path}/{version}/v2/schedules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSchedule(body, callback)</td>
    <td style="padding:15px">Create Schedule</td>
    <td style="padding:15px">{base_path}/{version}/v2/schedules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSchedule(identifier, identifierType = 'id', callback)</td>
    <td style="padding:15px">Get Schedule</td>
    <td style="padding:15px">{base_path}/{version}/v2/schedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSchedule(identifier, identifierType = 'id', body, callback)</td>
    <td style="padding:15px">Update Schedule (Partial)</td>
    <td style="padding:15px">{base_path}/{version}/v2/schedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSchedule(identifier, identifierType = 'id', callback)</td>
    <td style="padding:15px">Delete Schedule</td>
    <td style="padding:15px">{base_path}/{version}/v2/schedules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScheduleTimeline(identifier, identifierType = 'id', expand, interval, intervalUnit = 'days', date, callback)</td>
    <td style="padding:15px">Get Schedule Timeline</td>
    <td style="padding:15px">{base_path}/{version}/v2/schedules/{pathv1}/timeline?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportSchedule(identifier, identifierType = 'id', callback)</td>
    <td style="padding:15px">Export Schedule</td>
    <td style="padding:15px">{base_path}/{version}/v2/schedules/{pathv1}.ics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createScheduleRotation(identifier, scheduleIdentifierType = 'id', body, callback)</td>
    <td style="padding:15px">Create Schedule Rotation</td>
    <td style="padding:15px">{base_path}/{version}/v2/schedules/{pathv1}/rotations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listScheduleRotations(identifier, scheduleIdentifierType = 'id', callback)</td>
    <td style="padding:15px">List Schedule Rotations</td>
    <td style="padding:15px">{base_path}/{version}/v2/schedules/{pathv1}/rotations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScheduleRotation(identifier, scheduleIdentifierType = 'id', id, callback)</td>
    <td style="padding:15px">Get Schedule Rotation</td>
    <td style="padding:15px">{base_path}/{version}/v2/schedules/{pathv1}/rotations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateScheduleRotation(identifier, scheduleIdentifierType = 'id', id, body, callback)</td>
    <td style="padding:15px">Update Schedule Rotation (Partial)</td>
    <td style="padding:15px">{base_path}/{version}/v2/schedules/{pathv1}/rotations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteScheduleRotation(identifier, scheduleIdentifierType = 'id', id, callback)</td>
    <td style="padding:15px">Delete Schedule Rotation</td>
    <td style="padding:15px">{base_path}/{version}/v2/schedules/{pathv1}/rotations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createScheduleOverride(identifier, scheduleIdentifierType = 'id', body, callback)</td>
    <td style="padding:15px">Create Schedule Override</td>
    <td style="padding:15px">{base_path}/{version}/v2/schedules/{pathv1}/overrides?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listScheduleOverride(identifier, scheduleIdentifierType = 'id', callback)</td>
    <td style="padding:15px">List Schedule Overrides</td>
    <td style="padding:15px">{base_path}/{version}/v2/schedules/{pathv1}/overrides?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScheduleOverride(identifier, alias, scheduleIdentifierType = 'id', callback)</td>
    <td style="padding:15px">Get Schedule Override</td>
    <td style="padding:15px">{base_path}/{version}/v2/schedules/{pathv1}/overrides/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateScheduleOverride(identifier, alias, scheduleIdentifierType = 'id', body, callback)</td>
    <td style="padding:15px">Update Schedule Override</td>
    <td style="padding:15px">{base_path}/{version}/v2/schedules/{pathv1}/overrides/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteScheduleOverride(identifier, alias, scheduleIdentifierType = 'id', callback)</td>
    <td style="padding:15px">Delete Schedule Override</td>
    <td style="padding:15px">{base_path}/{version}/v2/schedules/{pathv1}/overrides/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listOnCalls(flat, date, callback)</td>
    <td style="padding:15px">List On Calls</td>
    <td style="padding:15px">{base_path}/{version}/v2/schedules/on-calls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOnCalls(identifier, scheduleIdentifierType = 'id', flat, date, callback)</td>
    <td style="padding:15px">Get On Calls</td>
    <td style="padding:15px">{base_path}/{version}/v2/schedules/{pathv1}/on-calls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNextOnCalls(identifier, scheduleIdentifierType = 'id', flat, date, callback)</td>
    <td style="padding:15px">Get Next On Calls</td>
    <td style="padding:15px">{base_path}/{version}/v2/schedules/{pathv1}/next-on-calls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportOnCallUser(identifier, callback)</td>
    <td style="padding:15px">Export On-Call User</td>
    <td style="padding:15px">{base_path}/{version}/v2/schedules/on-calls/{pathv1}.ics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listEscalations(callback)</td>
    <td style="padding:15px">List Escalations</td>
    <td style="padding:15px">{base_path}/{version}/v2/escalations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createEscalation(body, callback)</td>
    <td style="padding:15px">Create Escalation</td>
    <td style="padding:15px">{base_path}/{version}/v2/escalations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEscalation(identifier, identifierType = 'id', callback)</td>
    <td style="padding:15px">Get Escalation</td>
    <td style="padding:15px">{base_path}/{version}/v2/escalations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteEscalation(identifier, identifierType = 'id', callback)</td>
    <td style="padding:15px">Delete Escalation</td>
    <td style="padding:15px">{base_path}/{version}/v2/escalations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateEscalation(identifier, identifierType = 'id', body, callback)</td>
    <td style="padding:15px">Update Escalation (Partial)</td>
    <td style="padding:15px">{base_path}/{version}/v2/escalations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listForwardingRules(callback)</td>
    <td style="padding:15px">List Forwarding Rules</td>
    <td style="padding:15px">{base_path}/{version}/v2/forwarding-rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createForwardingRule(body, callback)</td>
    <td style="padding:15px">Create Forwarding Rule</td>
    <td style="padding:15px">{base_path}/{version}/v2/forwarding-rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getForwardingRule(identifier, identifierType = 'id', callback)</td>
    <td style="padding:15px">Get Forwarding Rule</td>
    <td style="padding:15px">{base_path}/{version}/v2/forwarding-rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteForwardingRule(identifier, identifierType = 'id', callback)</td>
    <td style="padding:15px">Delete Forwarding Rule</td>
    <td style="padding:15px">{base_path}/{version}/v2/forwarding-rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateForwardingRule(identifier, identifierType = 'id', body, callback)</td>
    <td style="padding:15px">Update Forwarding Rule</td>
    <td style="padding:15px">{base_path}/{version}/v2/forwarding-rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCustomUserRoles(callback)</td>
    <td style="padding:15px">List Custom User Roles</td>
    <td style="padding:15px">{base_path}/{version}/v2/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCustomUserRole(body, callback)</td>
    <td style="padding:15px">Create Custom User Role</td>
    <td style="padding:15px">{base_path}/{version}/v2/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomUserRole(identifier, identifierType = 'id', callback)</td>
    <td style="padding:15px">Get Custom User Role</td>
    <td style="padding:15px">{base_path}/{version}/v2/roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCustomUserRole(identifier, identifierType = 'id', callback)</td>
    <td style="padding:15px">Delete Custom User Role</td>
    <td style="padding:15px">{base_path}/{version}/v2/roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCustomUserRole(identifier, identifierType = 'id', body, callback)</td>
    <td style="padding:15px">Update Custom User Role</td>
    <td style="padding:15px">{base_path}/{version}/v2/roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPolicy(body, teamId, callback)</td>
    <td style="padding:15px">Create Policy</td>
    <td style="padding:15px">{base_path}/{version}/v2/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getV2PoliciesAlert(teamId, callback)</td>
    <td style="padding:15px">List Alert Policies</td>
    <td style="padding:15px">{base_path}/{version}/v2/policies/alert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listNotificationPolicies(teamId, callback)</td>
    <td style="padding:15px">List Notification Policies</td>
    <td style="padding:15px">{base_path}/{version}/v2/policies/notification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicy(policyId, teamId, callback)</td>
    <td style="padding:15px">Delete Policy</td>
    <td style="padding:15px">{base_path}/{version}/v2/policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicy(policyId, teamId, callback)</td>
    <td style="padding:15px">Get Policy</td>
    <td style="padding:15px">{base_path}/{version}/v2/policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePolicy(policyId, teamId, body, callback)</td>
    <td style="padding:15px">Update Policy</td>
    <td style="padding:15px">{base_path}/{version}/v2/policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enablePolicy(policyId, teamId, callback)</td>
    <td style="padding:15px">Enable Policy</td>
    <td style="padding:15px">{base_path}/{version}/v2/policies/{pathv1}/enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disablePolicy(policyId, teamId, callback)</td>
    <td style="padding:15px">Disable Policy</td>
    <td style="padding:15px">{base_path}/{version}/v2/policies/{pathv1}/disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changePolicyOrder(policyId, teamId, body, callback)</td>
    <td style="padding:15px">Change Policy Order</td>
    <td style="padding:15px">{base_path}/{version}/v2/policies/{pathv1}/change-order?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIncidentRequestStatus(requestId, callback)</td>
    <td style="padding:15px">Get Request Status of Incident</td>
    <td style="padding:15px">{base_path}/{version}/v1/incidents/requests/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIncident(body, callback)</td>
    <td style="padding:15px">Create Incident</td>
    <td style="padding:15px">{base_path}/{version}/v1/incidents/create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIncident(identifier, identifierType = 'id', callback)</td>
    <td style="padding:15px">Get Incident</td>
    <td style="padding:15px">{base_path}/{version}/v1/incidents/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIncident(identifier, identifierType = 'id', callback)</td>
    <td style="padding:15px">Delete Incident</td>
    <td style="padding:15px">{base_path}/{version}/v1/incidents/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listIncidents(query, offset, limit, sort = 'createdAt', order = 'asc', callback)</td>
    <td style="padding:15px">List incidents</td>
    <td style="padding:15px">{base_path}/{version}/v1/incidents/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">closeIncident(identifier, identifierType = 'id', body, callback)</td>
    <td style="padding:15px">Close Incident</td>
    <td style="padding:15px">{base_path}/{version}/v1/incidents/{pathv1}/close?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
