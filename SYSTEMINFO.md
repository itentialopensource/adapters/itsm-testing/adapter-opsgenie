# Atlassian Opsgenie

Vendor: Atlassian
Homepage: https://www.atlassian.com/

Product: Opsgenie
Product Page: https://www.atlassian.com/software/opsgenie

## Introduction
We classify Opsgenie into the ITSM (Service Management) domain as Opsgenie provides ticketing solutions for Incident and Problem Management.

"Centralize alerts and notify the right people at the right time" 
"Opsgenie groups alerts, filters the noise, and notifies you using multiple notification channels." 

## Why Integrate
The Opsgenie adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Opsgenie. With this adapter you have the ability to perform operations such as:

- Policy
- Incident

## Additional Product Documentation
The [API documents for Opsgenie](https://support.atlassian.com/opsgenie/docs/what-is-a-default-api-integration/)
The [Create API for Opsgenie](https://support.atlassian.com/opsgenie/docs/create-a-default-api-integration/)
The [Opsgenie API Key](https://confluence.atlassian.com/servicemanagementserver050/getting-opsgenie-api-information-1142253095.html)
